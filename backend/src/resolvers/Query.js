  const { forwardTo } = require('prisma-binding');
const { hasPermission } = require('../utils');

const checkIfUserIsLoggedIn = ctx => {
      if(!ctx.request.userId){
      throw new Error('not logged in');
    }
}

const Query = {
  items: forwardTo('db'),
  item: forwardTo('db'),
  itemsConnection: forwardTo('db'),
  me(parent, args, ctx, info){
    if(!ctx.request.userId){
      return null;
    }
    return ctx.db.query.user({
      where: {
        id: ctx.request.userId
      },
    }, info);
  },

  users(parent, args, ctx, info) {
    //check if logged in
    checkIfUserIsLoggedIn(ctx);

    hasPermission(ctx.request.user, ['ADMIN', 'PERMISSIONUPDATE']);

    return ctx.db.query.users({}, info)
  },

  async order(parent, args, ctx, info) {
    checkIfUserIsLoggedIn(ctx);
    const order = await ctx.db.query.order({where: { id: args.id }}, info);
    if(!order) {
      throw new Error(`no such order with id ${args.id}`)
    }
    const ownsOrder = order.user.id === ctx.request.userId;
    const hasPermissionToSeeOrder = ctx.request.user.permissions.includes("ADMIN");

    if(!ownsOrder || !hasPermissionToSeeOrder) {
      throw new Error('Sorry you cannot see this order');
    }

    return order;
  },

  async orders(parent, args, ctx, info) {
    const { userId } = ctx.request;
    checkIfUserIsLoggedIn(ctx);
    const hasPermissionToSeeOrders = ctx.request.user.permissions.includes("ADMIN");
    if(!hasPermissionToSeeOrders) {
      throw new Error('Sorry you cannot see orders');
    }

    const orders = await ctx.db.query.orders({
      where: { user: {id: userId}}
    }, info);

    return orders;
  }
};

module.exports = Query;
