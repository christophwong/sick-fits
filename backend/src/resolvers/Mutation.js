const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { randomBytes } = require('crypto');
const { promisify } = require('util');
const { hasPermission } = require('../utils');
const {transport, makeANiceEmail} = require('../mail');
const stripe = require('../stripe');

const throwErrorIfNotSignedIn = ctx => {
  if(!ctx.request.userId) {
    throw new Error('You must sign in before doing this!');
  }
}

const Mutations = {
  async createItem(parent, args, ctx, info) {
    throwErrorIfNotSignedIn(ctx);
    const item = await ctx.db.mutation.createItem(
      {
        data: {
          user: {
            connect: {
              id: ctx.request.userId,
            },
          },
          ...args,
        },
      },
      info
      );

    return item;
  },

  updateItem(parent, args, ctx, info){
    const updates = {...args};
    delete updates.id

    return ctx.db.mutation.updateItem({
      data: updates,
      where: {id: args.id},
    },
    info
    );
  },

  async deleteItem(parent, args, ctx, info){
    const where = {id: args.id};
    const item = await ctx.db.query.item({where}, `{id title user{id}}`);
    // TODO authenticate
    // check if owner owns it
    const ownsItem = ctx.request.userId === item.user.id;
    const userHasPermission = ctx.request.user.permissions.some(permission => (
      ['ADMIN', 'ITEMDELETE'].includes(permission)
      ));

    if(!ownsItem && !userHasPermission){
      throw new Error('You don\'t have the permissions');
    }
    // check if user has priviliege
    return ctx.db.mutation.deleteItem({where}, info);
  },

  async signup(parent, args, ctx, info){
    args.email = args.email.toLowerCase();
    //hash the passwords!
    const password = await bcrypt.hash(args.password, 10);
    const user = await ctx.db.mutation.createUser(
      {
        data: {
          ...args,
          password,
          permissions: { set: ['USER'] }
        }
      }, info
      );
    const token = jwt.sign({ userId: user.id}, process.env.APP_SECRET);
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365, // one year cookie
    });

    return user;

  },

  async signin(parent, {email, password}, ctx, info) {
    const user = await ctx.db.query.user({where: {email}});
    if(!user) {
      throw new Error(`No such account with email ${email}`);
    }
    const valid = await bcrypt.compare(password, user.password);
    if(!valid){
      throw new Error(`invalid login`);
    }

    const token = jwt.sign({userId: user.id}, process.env.APP_SECRET);
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365, // one year cookie
    });

    return user;
  },

  signout(parents, args, ctx, info){
    ctx.response.clearCookie('token');
    return {message: "Good bye"};
  },


  async requestReset(parents, args, ctx, info){
    //1. check if real user
    const user = await ctx.db.query.user({where: {email: args.email}})
    if(!user) {
      throw new Error(`No such account with email ${args.email}`);
    }
    //2. set reset token and expiry
    const promisifiedRandomBytes = promisify(randomBytes);
    const resetToken = (await promisifiedRandomBytes(20)).toString('hex');
    const resetTokenExpiry = Date.now() + 3600000; //1 hour from now
    const res = await ctx.db.mutation.updateUser({
      where: {email: args.email},
      data: {resetToken, resetTokenExpiry}
    });

    //3. email token
    const mailRes = await transport.sendMail({
      from: 'ctophw@test.com',
      to: user.email,
      subject: 'your password reset email',
      html: makeANiceEmail(`Your password reset token is here\n\n
        <a href="${process.env.FRONTEND_URL}/reset?resetToken=${resetToken}">Click here to reset your password</a>`)
    })
    return {message: "Thanks"};

  },
  async resetPassword(parents, args, ctx, info){

    //args resetToekn, password
    // const user = await ctx.db.query.user({where: {resetToken: args.resetToken}})
    if (args.password !== args.confirmPassword){
      throw new Error('Your passwords don\'t match!');
    }
    const [user] = await ctx.db.query.users({
      where: {
        resetToken: args.resetToken,
        resetTokenExpiry_gte: Date.now() - 3600000,
      },
    });

    if(!user) { throw new Error('reset token is invalid or expired!');}

    const password = await bcrypt.hash(args.password, 10);

    const updatedUser = await ctx.db.mutation.updateUser({
      where: {email: user.email},
      data: {
        password,
        resetToken: null,
        resetTokenExpiry: null,
      }});

    const token = jwt.sign({ userId: user.id}, process.env.APP_SECRET);
    ctx.response.cookie('token', token, {
      httpOnly: true,
      maxAge: 1000 * 60 * 60 * 24 * 365, // one year cookie
    });

    return updatedUser;
  },

  async updatePermissions(parents, args, ctx, info) {
    // check if logged in
    if(!ctx.request.userId) {
      throw new Error("Must be logged in to do this!");
    }

    // query user
    const user = await ctx.db.query.user({where: {id: ctx.request.userId,}}, info);

    // check if allowed
    hasPermission(ctx.request.user, ["ADMIN", 'PERMISSIONUPDATE']);
    // udpate permissions

    return ctx.db.mutation.updateUser({
      data: {
        permissions: {
          set: args.permissions,
        },
      },
      where: {id: args.userId},
    },
    info
    );
  },

  async addToCart(parent, args, ctx, info) {
    const { userId } = ctx.request;
    if(!userId) {throw new Error('must sign in')}
      const [existingCartItem] = await ctx.db.query.cartItems({
        where: {
          user: {id: userId},
          item: {id: args.id },
        }, info
      });

    if(existingCartItem){
      console.log('This item is already in your cart');
      return ctx.db.mutation.updateCartItem({
        where: {id: existingCartItem.id },
        data: { quantity: existingCartItem.quantity + 1},
        info});
    }

    return ctx.db.mutation.createCartItem({
      data: {
        user: {
          connect: { id: userId },
        },
        item: {
          connect: {id: args.id}
        }
      },
      info
    });

  },

  async removeFromCart(parent, args, ctx, info){
    const { userId } = ctx.request;
    // if(!userId) {throw new Error('must sign in')}
    const cartItem = await ctx.db.query.cartItem(
      {
        where: {id: args.id},

      }, `{id, user {id}}`);

    if(!cartItem) throw new Error('no cart item');
    if(cartItem.user.id !== userId) throw new Error('cheating?');
    return ctx.db.mutation.deleteCartItem({
      where: {id: args.id}
    }, info)

  },

  async createOrder(parents, args, ctx, info) {
    // query user make sure signed in
    throwErrorIfNotSignedIn(ctx);
    const {userId} = ctx.request;
    const user = await ctx.db.query.user({where: {id: userId}},
      `{
        id
        email
        name
        cart
        {
          id
          quantity
          item
          {
            title
            price
            id
            image
            largeImage
            description
          }
        }
      }`)
    const amount = user.cart.reduce((tally, cartItem) => (tally + cartItem.item.price * cartItem.quantity), 0);

    const charge = await stripe.charges.create({
      amount,
      currency: 'USD',
      source: args.token,
    });

    const orderItems = user.cart.map((cartItem) => {
      const orderItem = {
        ...cartItem.item,
        quantity: cartItem.quantity,
        user: {connect: {id: userId}}
      }
      delete orderItem.id
      return orderItem;
    });
// create order

    const order = await ctx.db.mutation.createOrder({
      data: {
        total: charge.amount,
        charge: charge.id,
        items: { create: orderItems },
        user: { connect: {id: userId}},
      }
    })
    //clean up cart, clean user's cart, delete cart items
    const cartItemIds = user.cart.map(cartItem => cartItem.id);
    await ctx.db.mutation.deleteManyCartItems({
      where: {
        id_in: cartItemIds,
      },
    })
    return order;

  },

};

module.exports = Mutations;
