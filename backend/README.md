# Sick Fits (Backend) - a React/Graphql demo online store web-app

## Live Demo \([Link](https://sfits-yoga-prod.herokuapp.com)\)

Follow the link in the title for the backend app (the backend graphql playground) hosted on Heroku.

## Running this app locally
```
cd backend/
```

This app depends on some environmental variables that are not checked into this git repo.
To provide the variables, make a file named 'variables.env' at the root of the backend project directory and provide these variables:

```
FRONTEND_URL
PRISMA_ENDPOINT
PRISMA_SECRET
APP_SECRET
STRIPE_SECRET
PORT
MAIL_HOST
MAIL_PORT
MAIL_USER
MAIL_PASS
```

For `PRISMA_ENDPOINT` and `PRISMA_SECRET` you will need to sign up on prisma.io and can setup a hosted service or use their demo dev servers for an API key.

`APP_SECRET` should be a secret string to use with JWT tokens for encrypting and salting session cookies.

And temporary `MAIL_*` variables can be obtained via a dev email service on mailtrap.io .

```
npm install
npm run dev
open http://localhost:7777
```
