# Sick Fits - a React/Graphql demo online store web-app

## Live Demo \([Link](https://sfits-react-prod.herokuapp.com)\)

Follow the link in the title for the frontend app hosted on Heroku.

The backend is also hosted on Heroku as a seprate service. 
Here is a url to the backend graphql playground (https://sfits-yoga-prod.herokuapp.com)

## Demo statement

This is a web application that demonstrate basic online store functionalities using the following technologies:
- [Javascript](http://www.ecma.ch/)
- [React](https://reactjs.org/)
- [Next JS](https://nextjs.org/)
- [Apollo client](https://www.apollographql.com/)
- [GraphQL-Yoga](https://github.com/prisma/graphql-yoga)
- [Prisma](https://www.prisma.io/)
- [GraphQL](https://graphql.org/)

## Running this app
This repository contains both the frontend and backend apps. These are two seperate apps and instructions to run them are in the READMEs in the respective directories.
