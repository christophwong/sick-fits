# Sick Fits (Frontend) - a React/Graphql demo online store web-app

## Live Demo \([Link](https://sfits-react-prod.herokuapp.com)\)

Follow the link in the title for the frontend app hosted on Heroku.

## Running this app locally
```
cd frontend/
npm install
npm run dev
open http://localhost:7777
```