import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';
import wait from 'waait';
import { MockedProvider } from 'react-apollo/test-utils';
import { fakeOrder } from '../lib/testUtils';
import Order, { SINGLE_ORDER_QUERY } from '../components/Order';

const testId = 'abc4343';
const mocks = [{
  request: {
   query: SINGLE_ORDER_QUERY,
   variables: {
    id: testId,
   },
 },
 result: {
  data: {
    order: { ...fakeOrder(), id: testId }
  }
 }
}]

describe('<Order/>', () => {
  it('renders and matches snap shot', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <Order id={ testId }/>
      </MockedProvider>
      );
    await wait();
    wrapper.update();
    const orderComponent = wrapper.find('div[data-test="order"]');

    expect(toJSON(orderComponent)).toMatchSnapshot();
  });
})