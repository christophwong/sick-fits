import { mount } from 'enzyme';
import wait from 'waait';
import toJSON from 'enzyme-to-json';
import Router from 'next/router';
import { MockedProvider } from 'react-apollo/test-utils';
import CreateItem, { CREATE_ITEM_MUTATION } from '../components/CreateItem';
import { fakeItem } from '../lib/testUtils';

// mock global fetch api
global.fetch = jest.fn().mockResolvedValue({
  json: () => ({
    secure_url: fakeItem().image,
    eager: [{secure_url: fakeItem().largeImage}]
  })
})

describe('<CreateItem/>', () => {
  it('renders and matches snap', async () => {
    const wrapper = mount(
      <MockedProvider>
        <CreateItem/>
        </MockedProvider>
        );
    const form = wrapper.find('form[data-test="create-item-form"]');
    expect(toJSON(form)).toMatchSnapshot();
  });

  it('uploads a file when changed', async () => {
    const wrapper = mount(
      <MockedProvider>
        <CreateItem/>
        </MockedProvider>
        );

    const input = wrapper.find('input[type="file"]');
    input.simulate('change', { target: { files: ['doggo.ext']}});

    await wait();

    const compo = wrapper.find('CreateItem').instance();
    expect(compo.state.image).toBe(fakeItem().image);
    expect(compo.state.largeImage).toBe(fakeItem().largeImage);
    expect(global.fetch).toHaveBeenCalled();
    global.fetch.mockReset();
  });

  it('handles state updating', async () => {
   const wrapper = mount(
    <MockedProvider>
      <CreateItem/>
      </MockedProvider>
      );

   const stateAfterChange = {
    title: 'title string',
    price: 50000,
    description: 'so nice',
  }

  wrapper
  .find('#title')
  .simulate('change', { target: { value: stateAfterChange.title, name: 'title'}});

  wrapper
  .find('#price')
  .simulate('change', { target: { value: stateAfterChange.price, name: 'price', type: 'number'}});

  wrapper
  .find('#description')
  .simulate('change', { target: { value: stateAfterChange.description, name: 'description'}});
    await wait();
    wrapper.update();
  expect(wrapper.find('CreateItem').instance().state)
  .toMatchObject(stateAfterChange)
});

  it('creates an item when form is submitted', async () => {
    const item = fakeItem();

    const mocks = [{
      request: {query: CREATE_ITEM_MUTATION,
        variables: {
         title: item.title,
         description: item.description,
         price: item.price,
         image: '',
         largeImage: '',
       }},
       result: {
        data: {
          createItem: {
            ...item,
            __typename: 'Item'
          }
        }
       }
     }]

     const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <CreateItem/>
        </MockedProvider>
        );

     wrapper
     .find('#title')
     .simulate('change', { target: { value: item.title, name: 'title'}});

     wrapper
     .find('#price')
     .simulate('change', { target: { value: item.price, name: 'price', type: 'number'}});

     wrapper
     .find('#description')
     .simulate('change', { target: { value: item.description, name: 'description'}});

     Router.router = { push: jest.fn() };
     wrapper.find('form').simulate('submit');
     await wait();
     expect(Router.router.push).toHaveBeenCalledWith({pathname: "/item", query: {id: item.id}});
   });
});