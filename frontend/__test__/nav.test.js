import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';
import wait from 'waait';
import Nav from '../components/Nav';
import { CURRENT_USER_QUERY } from '../components/User';
import { MockedProvider} from 'react-apollo/test-utils';
import { fakeUser, fakeCartItem } from '../lib/testUtils';

const notSignedInMocks = [{
  request: {query: CURRENT_USER_QUERY},
  result: {data: {me: null}}
}]

const signedInMocks = [{
  request: { query: CURRENT_USER_QUERY },
  result: {
    data: {
      me: fakeUser()
    },
  },
}]

const signedInMocksWithItems = [{
  request: { query: CURRENT_USER_QUERY },
  result: {
    data: {
      me: {...fakeUser(), cart: [fakeCartItem(), fakeCartItem(), fakeCartItem()]}
    },
  },
}]

describe('<Nav/>', () => {
  it('renders a minimal nav when not signed in', async () => {
      const wrapper = mount(
      <MockedProvider mocks={notSignedInMocks}>
        <Nav/>
      </MockedProvider>);
      const navComponent = wrapper.find('ul[data-test="nav"]');
      expect(toJSON(navComponent)).toMatchSnapshot();
  });

  it('renders whole nav when signed in', async () => {
      const wrapper = mount(
      <MockedProvider mocks={signedInMocks}>
        <Nav/>
      </MockedProvider>);

      await wait();
      wrapper.update();
      const navComponent = wrapper.find('ul[data-test="nav"]');
      expect(navComponent.children().length).toBe(6);
      expect(navComponent.text()).toContain('Shop');
      expect(navComponent.text()).toContain('Sell');
      expect(navComponent.text()).toContain('Orders');
      expect(navComponent.text()).toContain('Account');
      expect(navComponent.text()).toContain('Sign Out');
  });

  it('renders amount of items in cart', async () => {
      const wrapper = mount(
      <MockedProvider mocks={signedInMocksWithItems}>
        <Nav/>
      </MockedProvider>);

      await wait();
      wrapper.update();
      const counter = wrapper.find('div.count');
      expect(counter.text()).toBe("9");
  });
})