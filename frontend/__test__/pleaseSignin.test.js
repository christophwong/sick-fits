import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';
import wait from 'waait';
import PleaseSignin from '../components/PleaseSignin';
import { CURRENT_USER_QUERY } from '../components/User';
import { MockedProvider} from 'react-apollo/test-utils';
import { fakeUser } from '../lib/testUtils';

const notSignedInMocks = [{
  request: {query: CURRENT_USER_QUERY},
  result: {data: {me: null}}
}]

const signedInMocks = [{
  request: { query: CURRENT_USER_QUERY },
  result: {
    data: {
      me: fakeUser()
    },
  },
}]

describe('<PleaseSignin/>', () => {
  it('renders the sign in dialog to signed out users', async () => {
    const wrapper = mount(
      <MockedProvider mocks={notSignedInMocks}>
        <PleaseSignin />
      </MockedProvider>
    );

    await wait();
    wrapper.update();

    expect(wrapper.text()).toContain("Please Sign in");
    expect(wrapper.find('Signin').exists()).toBe(true);

  });

  it('renders the child component when signed in', async () => {
    const TestChildComponent = () => (<p>child</p>);
    const wrapper = mount(
      <MockedProvider mocks={signedInMocks}>
        <PleaseSignin>
          <TestChildComponent/>
        </PleaseSignin>
      </MockedProvider>
      );

    await wait();
    wrapper.update();

    expect(wrapper.contains(<TestChildComponent/>)).toBe(true);

  });
});