import { mount } from 'enzyme';
import toJSON from 'enzyme-to-json';
import wait from 'waait';
import Router from 'next/router';
import Pagination, { PAGINATION_QUERY } from '../components/Pagination';
import { MockedProvider} from 'react-apollo/test-utils';

Router.router = {
  push() {},
  prefetch() {},
}

function makeMocksFor(length){
  return [
  {
    request: {query: PAGINATION_QUERY},
    result: {
      data: {
        itemsConnection: {
          __typename: "aggregate",
          aggregate: {
            __typename: "count",
            count: length
          }
        }
      }
    }}
  ]
} 
describe('<Pagination/>', () => {
  it('displays a loading message', () => {
    const wrapper = mount(<MockedProvider mocks={makeMocksFor(1)}>
      <Pagination page={1}></Pagination>
    </MockedProvider>);

    expect(wrapper.text()).toContain('Loading...');
  })

  it('renders pagination for 18 items', async () => {
    const itemCount = 18;
    const wrapper = mount(<MockedProvider mocks={makeMocksFor(itemCount)}>
      <Pagination page={1}></Pagination>
    </MockedProvider>);
    await wait();
    wrapper.update();

    expect(wrapper.find('.total-pages').text()).toBe(Math.ceil(itemCount/4).toString());

    const pagination = wrapper.find('div[data-test="pagination"]')
    expect(toJSON(pagination)).toMatchSnapshot();
  });

  it('disables prev button on first page', async () => {
    const itemCount = 18;
    const wrapper = mount(<MockedProvider mocks={makeMocksFor(itemCount)}>
      <Pagination page={1}></Pagination>
    </MockedProvider>);
    await wait();
    wrapper.update();

    expect(wrapper.find('a.prev').prop('aria-disabled')).toBe(true);
    expect(wrapper.find('a.next').prop('aria-disabled')).toBe(false);
  });
  it('disables next button on last page', async () => {
    const itemCount = 18;
    const lastPage = 5

    const wrapper = mount(<MockedProvider mocks={makeMocksFor(itemCount)}>
      <Pagination page={lastPage}></Pagination>
    </MockedProvider>);
    await wait();
    wrapper.update();

    expect(wrapper.find('a.next').prop('aria-disabled')).toBe(true);
    expect(wrapper.find('a.prev').prop('aria-disabled')).toBe(false);
  });

  it('enables all buttons on a middle page', async () => {
    const itemCount = 18;
    const randomMiddlePage = 2;
    const wrapper = mount(<MockedProvider mocks={makeMocksFor(itemCount)}>
      <Pagination page={randomMiddlePage}></Pagination>
      </MockedProvider>);
    await wait();
    wrapper.update();

    expect(wrapper.find('a.next').prop('aria-disabled')).toBe(false);
    expect(wrapper.find('a.prev').prop('aria-disabled')).toBe(false);
  });
})