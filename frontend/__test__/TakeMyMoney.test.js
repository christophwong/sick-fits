import {mount} from 'enzyme';
import wait from 'waait';
import toJSON from 'enzyme-to-json';
import NProgress from 'nprogress';
import Router from 'next/router';
import { MockedProvider } from 'react-apollo/test-utils';
import { ApolloConsumer } from 'react-apollo';
import TakeMyMoney, { CREATE_ORDER_MUTATION } from '../components/TakeMyMoney';
import { CURRENT_USER_QUERY } from '../components/User';
import {fakeUser, fakeCartItem} from '../lib/testUtils';

Router.router = { push(){} };

const mocks = [{
  request: { query: CURRENT_USER_QUERY },
  result: {
    data: {
      me: {
        ...fakeUser(),
        cart: [fakeCartItem()],
      },
    },
  },
}];

describe('<TakeMyMoney/>', () => {
  it('renders and snaps', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <TakeMyMoney />
      </MockedProvider>
      );
    await wait();
    wrapper.update();
    expect(toJSON(wrapper.find('ReactStripeCheckout'))).toMatchSnapshot();
  });

  it('creates an order on token', () => {
    const createOrderMock = jest.fn().mockResolvedValue({
      data: { createOrder: { id: 'random'}}
    });

    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <TakeMyMoney />
      </MockedProvider>
      );

    const component = wrapper.find('TakeMyMoney').instance();

    component.onToken({id: 'asdasd'}, createOrderMock);

    expect(createOrderMock).toHaveBeenCalled();
    expect(createOrderMock).toHaveBeenCalledWith({"variables": {"token": "asdasd"}});
  });

  it('starts the progress bar', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <TakeMyMoney />
      </MockedProvider>
      );

    await wait();
    wrapper.update();

    NProgress.start = jest.fn()
    const createOrderMock = jest.fn().mockResolvedValue({
      data: { createOrder: { id: '123A'}}
    });
    const component = wrapper.find('TakeMyMoney').instance();
    component.onToken({id: 'asdasd'}, createOrderMock);

    expect(NProgress.start).toHaveBeenCalled();
  });

  it('routes to order page when completed', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <TakeMyMoney/>
        </MockedProvider>
        );
    
    await wait();
    wrapper.update();

    const createOrderId = 'abc123';
    const createOrderMock = jest.fn().mockResolvedValue({
      data: { createOrder: { id: createOrderId}}
    });
    const component = wrapper.find('TakeMyMoney').instance();
    Router.router.push = jest.fn();
    
    component.onToken({id: 'asd'}, createOrderMock);

    await wait();
    expect(Router.router.push).toHaveBeenCalledWith({
      pathname: '/order',
      query: {
      id: createOrderId
    }})

  })
})