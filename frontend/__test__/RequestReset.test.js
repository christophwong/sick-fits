import { mount } from 'enzyme';
import wait from 'waait';
import toJSON from 'enzyme-to-json';
import { MockedProvider } from 'react-apollo/test-utils';
import RequestReset, { REQUEST_RESET_MUTATION } from '../components/RequestReset';

const emailValue = 'tea@test.com';

const mocks = [{
  request: {
    query: REQUEST_RESET_MUTATION,
    variables: {email: emailValue}
  },
  result: {
    data: {
      requestReset: {
        __typename: 'Message',
        message: "Success"
      }
    }
  }
}]

describe('<RequestReset/>', () => {
  it('renders and matches snap', () => {
    const wrapper = mount(
      <MockedProvider>
        <RequestReset />
      </MockedProvider>
      );
    const form = wrapper.find('Form form');
    expect(toJSON(form)).toMatchSnapshot();
  });

  it('successful reset request displays message', async () => {
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <RequestReset />
      </MockedProvider>
      );

    wrapper
    .find('input')
    .simulate('change', { target: {
      name: 'email', 
      value: emailValue
    }});

    wrapper.find('Form form').simulate('submit');

    await wait();
    wrapper.update();
    
    expect(wrapper.find('#reset-success-message').text()).toContain('Success! Check your email for password reset link');
  })
});