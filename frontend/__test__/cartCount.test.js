import { shallow } from 'enzyme';
import CartCount from '../components/CartCount';
import toJSON from 'enzyme-to-json';

describe('<CartCount/>', () => {
  const wrapper = shallow(<CartCount count={10}/>)
  it('matches the snapshot', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
  })
})