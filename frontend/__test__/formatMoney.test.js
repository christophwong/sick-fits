import formatMoney from '../lib/formatMoney';

describe('formatMoney function', ()=>{
  it('can handle cents', () => {
    expect(formatMoney(1)).toEqual('$0.01');
    expect(formatMoney(10)).toEqual('$0.10');
    expect(formatMoney(9)).toEqual('$0.09');
    expect(formatMoney(40)).toEqual('$0.40');
  })

  it('leaves whole dollar as whole', () => {
    expect(formatMoney(1000)).toEqual('$10');
    expect(formatMoney(500000000)).toEqual('$5,000,000');
  })

  it('handles whole dollar with cents', () => {
    expect(formatMoney(1012)).toEqual('$10.12');
    expect(formatMoney(500012)).toEqual('$5,000.12');
  })
})