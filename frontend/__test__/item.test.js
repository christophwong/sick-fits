import Item from '../components/Item';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';

const fakeItem = {
  id: '123ABV',
  title: 'Fake Item',
  price: 5000,
  description: 'This item is described as fine',
  image: 'dog.jpg',
  largeImage: 'large-dog.jpg',
}

describe('<Item/>', () => {
    const wrapper = shallow(<Item item={fakeItem}/>);
  it('renders and matches snapshot', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
  })
// 
//   it('renders and display the images', () => {
//     const img = wrapper.find('img');
//     expect(img.props().src).toBe(fakeItem.image);
//     expect(img.props().alt).toBe(fakeItem.title);
//   });
// 
//   it('renders and displays properly', () => {
//     expect(wrapper.find('PriceTag').children().text()).toBe("$50");
//     expect(wrapper.find('Title a').text()).toBe(fakeItem.title);
//   });
// 
//   it('renders the button', () => {
//     const buttonList = wrapper.find('.buttonList');
//     expect(buttonList.children().length).toBe(3);
// 
//     expect(buttonList.find('Link').exists()).toBe(true);
//     expect(buttonList.find('AddToCart').exists()).toBe(true);
//     expect(buttonList.find('DeleteItem').exists()).toBe(true);
// 
//   })
});