import {mount} from 'enzyme';
import wait from 'waait';
import toJSON from 'enzyme-to-json';
import { MockedProvider } from 'react-apollo/test-utils';
import { ApolloConsumer } from 'react-apollo';
import { CURRENT_USER_QUERY } from '../components/User';
import RemoveFromCart, { REMOVE_FROM_CART_MUTATION } from '../components/RemoveFromCart';
import {fakeUser, fakeCartItem} from '../lib/testUtils';

const fakeId = 'abc123';

const mocks = [
{
  request:{query: CURRENT_USER_QUERY }, 
  result:{
    data: {
      me: {
        ...fakeUser(),
        cart: [fakeCartItem({id: fakeId})],
      },
    },
  },
},
{
  request: { query: REMOVE_FROM_CART_MUTATION, variables: {id: fakeId}},
  result: {
    data: {
      removeFromCart: {
        id: fakeId,
        __typename: "CartItem",
      },
    },
  },
},
];

describe('<RemoveFromCart/>', () => {
  it('renders and snaps', () => {
    const wrapper = mount(
      <MockedProvider>
        <RemoveFromCart id={fakeId}/>
      </MockedProvider>
      );

    expect(toJSON(wrapper.find('button'))).toMatchSnapshot();
  });

  it('removes item from cart', async () => {
    let apolloClient;
    const wrapper = mount(
      <MockedProvider mocks={mocks}>
        <ApolloConsumer>
          {client => {
            apolloClient = client;
            return <RemoveFromCart id={fakeId}/>;
            }}
        </ApolloConsumer>
      </MockedProvider>
      );

    const res = await apolloClient.query({query: CURRENT_USER_QUERY});
    const cart = res.data.me.cart;
    expect(cart).toHaveLength(1);
    expect(cart[0].item.price).toBe(5000);

    wrapper.find('button').simulate('click');
    await wait();

    const res2 = await apolloClient.query({query: CURRENT_USER_QUERY});
    const cart2 = res2.data.me.cart
    expect(cart2).toHaveLength(0);
  })
})
